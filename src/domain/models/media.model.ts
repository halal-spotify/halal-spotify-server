export class Media {
  id: number;
  name: string;
  imagePath: string;
  filePath: string;
  listensCount: number;
}
