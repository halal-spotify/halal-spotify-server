export class User {
  constructor(name: string, email: string, password: string) {
    this.name = name;
    this.email = email;
    this.password = password;
  }

  id: number;
  name: string;
  password: string;
  email: string;
}
