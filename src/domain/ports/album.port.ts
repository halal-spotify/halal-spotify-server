import { AlbumEntity } from "src/adapters/application/album/album.entity";
import { Album } from "../models/album.model";
import { Transaction } from "sequelize";

export interface AlbumPort {
  getAlbums(): Promise<AlbumEntity[]>;
  getSingleAlbum(albumID: number): Promise<AlbumEntity>;
  createAlbum(
    album: Album,
    userID: number,
    transaction?: Transaction
  ): Promise<AlbumEntity>;
  addMediaToAlbum(mediaID: number, albumID: number, transaction?: Transaction);
  deleteAlbums(albumID: number[]): Promise<void>;
  updateAlbum(album: Album, transaction?: Transaction): Promise<void>;
}
