import { User } from "../models/user.model";

export interface UserPort {
  getAllUsers(): Promise<User[]>;
  createUser(user: User): Promise<User>;
  getUser(email: string): Promise<User>;
  getSingleUser(id: number): Promise<User>;
}
