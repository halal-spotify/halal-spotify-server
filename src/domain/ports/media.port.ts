import { Transaction } from "sequelize";
import { Media } from "../models/media.model";

export interface MediaPort {
  createMedia(media: Media, userID: number): void;
  // updateMedia(media: Media, mediaID: string): Promise<Media>;
  updateMediaImg(imgPath: string, mediaID: number): void;
  // searchMedia(mediaName: string): Promise<Media | null>;
  getSingleMedia(medaiID: string): Promise<Media>;
  getMedia(mediaID: number): Promise<Media>;
  deleteMedia(mediaID: number, transaction?: Transaction): void;
  getAllMedias(): Promise<any[]>;
  addMediaCount(mediaID: string): Promise<void>;
}
