import { Module } from "@nestjs/common";

import { UserModule } from "./adapters/application/user/user.module";
import { DatabaseModule } from "./adapters/dao/postgres/db.module";

import { MediaModule } from "./adapters/application/media/media.module";
import { AlbumModule } from "./adapters/application/album/album.module";

@Module({
  imports: [DatabaseModule, UserModule, MediaModule, AlbumModule],
  controllers: [],
  providers: []
})
export class AppModule {}
