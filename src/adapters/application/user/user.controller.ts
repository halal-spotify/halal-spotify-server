import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Param,
  Post,
  Put
} from "@nestjs/common";
import * as bcrypt from "bcrypt";
import { ApiTags, ApiResponse } from "@nestjs/swagger";

import {
  UserDTO,
  ApiRgisterDTO,
  ApiLoginDTO,
  credentialsDTO,
  ApiStatsUsersDTO,
  ApiUsersDeleteDTO
} from "./user.dto";
import { UserRepository } from "../../dao/postgres/user.repository";
import { generateToken } from "./user.utils";
import { User } from "src/domain/models/user.model";
import { Sequelize } from "sequelize-typescript";

@ApiTags("auth")
@Controller("auth")
export class UsersController {
  constructor(
    private readonly userRepository: UserRepository,
    @Inject("SEQUELIZE")
    private readonly sequelize: Sequelize
  ) {}

  @Post("register")
  @ApiResponse({ status: 201, description: "User registered successfully" })
  @ApiResponse({ status: 403, description: "User already exists" })
  async registerUser(@Body() userDTO: UserDTO): Promise<ApiRgisterDTO> {
    //mapping DTO to User Model (from domain)
    const userModel = UserDTO.mapToModel(userDTO);
    if (
      userModel.email === undefined ||
      userModel.password === undefined ||
      userModel.name === undefined
    )
      throw new HttpException("Invalid data", HttpStatus.BAD_REQUEST);

    //check if user already exist on db
    const userFound = await this.userRepository.getUser(userModel.email);
    if (userFound)
      throw new HttpException("User already exists", HttpStatus.FORBIDDEN);

    // Hash the password before saving it
    const hashedPassword = await bcrypt.hash(userModel.password, 10);

    // Save the user with the hashed password
    await this.userRepository.createUser({
      ...userModel,
      password: hashedPassword
    });

    //generate token
    const token = generateToken({
      name: userModel.name,
      email: userModel.email
    });

    return {
      token,
      name: userModel.name
    };
  }

  @Post("login")
  @ApiResponse({ status: 200, description: "User logged in successfully" })
  @ApiResponse({ status: 401, description: "Invalid credentials" })
  async loginUser(
    @Body() { email, password }: credentialsDTO
  ): Promise<ApiLoginDTO> {
    if (!email || !password)
      throw new HttpException("Invalid data", HttpStatus.BAD_REQUEST);

    // Check if the user exists in the database
    const user = await this.userRepository.getUser(email);
    if (!user) {
      throw new HttpException("User does not exist", HttpStatus.UNAUTHORIZED);
    }

    // Compare the provided password with the hashed password in the database
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      throw new HttpException("Invalid credentials", HttpStatus.UNAUTHORIZED);
    }

    // Generate a token for the user
    const token = generateToken({
      name: user.name,
      email: user.email
    });

    return {
      token,
      name: user.name
    };
  }

  @Get("stats")
  async getMediaStats(): Promise<ApiStatsUsersDTO> {
    const totalUsers = await this.userRepository.getUsersStats();

    const date = new Date();
    const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    const totalUsersThisMonth = await this.userRepository.getUsersStats(
      firstDay.toISOString(),
      lastDay.toISOString()
    );

    const firstDayLastMonth = new Date(
      date.getFullYear(),
      date.getMonth() - 1,
      1
    );
    const lastDayLastMonth = new Date(date.getFullYear(), date.getMonth(), 0);
    const totalUsersLastMonth = await this.userRepository.getUsersStats(
      firstDayLastMonth.toISOString(),
      lastDayLastMonth.toISOString()
    );

    let percentageAddedUsers = "0.00%";
    if (parseInt(totalUsers) !== 0) {
      percentageAddedUsers =
        (
          ((parseInt(totalUsersThisMonth) - parseInt(totalUsersLastMonth)) *
            100) /
          parseInt(totalUsers)
        ).toFixed(2) + "%";
    }

    return {
      totalUsers,
      percentageAddedUsers
    };
  }

  // CRUD
  @Get("users")
  async getAllUsers(): Promise<User[]> {
    const users = await this.userRepository.getAllUsers();
    return users;
  }

  @Get("tenusers")
  async getTenUsers(): Promise<User[]> {
    const users = await this.userRepository.getAllUsers(10);
    return users;
  }

  @Get("users/:id")
  async getSingleUser(@Param("id") id: number): Promise<User> {
    const users = await this.userRepository.getSingleUser(id);
    return users;
  }

  @Put("users/:id")
  async updateSingleUser(
    @Param("id") id: number,
    @Body() updatedUser: UserDTO
  ): Promise<any> {
    const user = await this.userRepository.getSingleUser(id);
    if (!user) {
      throw new HttpException("Artist doesn't exist", HttpStatus.NOT_FOUND);
    }

    const newUser = {
      id: user.id,
      name: user.name,
      password: user.password,
      email: user.email
    };
    newUser.name = updatedUser.name || user.name;
    newUser.email = updatedUser.email || user.email;
    newUser.password = updatedUser.password || user.password;
    if (updatedUser.password || user.password)
      newUser.password = await bcrypt.hash(newUser.password, 10);

    const newUserData = await this.userRepository.updateUser(newUser, id);
    return newUserData[1][0];
  }

  @Delete("users")
  async deleteAlbum(
    @Body() { userIDs }: { userIDs: number[] }
  ): Promise<ApiUsersDeleteDTO> {
    if (!userIDs) {
      throw new HttpException("UserID(s) is required", HttpStatus.BAD_REQUEST);
    }

    const transaction = await this.sequelize.transaction();
    try {
      for (let i = 0; i < userIDs.length; i++) {
        const userMedias = await this.userRepository.getUserMedias(
          userIDs[i],
          transaction
        );
        if (userMedias > 0) {
          throw new HttpException(
            `This user, id: ${userIDs[i]}, has media(s) associated, please delete them first`,
            HttpStatus.BAD_REQUEST
          );
        }
        const userAlbums = await this.userRepository.getUserAlbums(
          userIDs[i],
          transaction
        );
        if (userAlbums > 0) {
          throw new HttpException(
            `This user, id: ${userIDs[i]}, has album(s) associated, please delete them first`,
            HttpStatus.BAD_REQUEST
          );
        }
        await this.userRepository.deleteUserMedia(userIDs[i], transaction);
        await this.userRepository.deleteUserAlbum(userIDs[i], transaction);
        await this.userRepository.deleteUser(userIDs[i], transaction);
      }

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw new HttpException(
        err || "Error while deleting user(s)",
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }

    return {
      data: userIDs,
      message: "User deleted successfully!"
    };
  }

  @Get("search/:userName")
  async getUsersByName(@Param("userName") userName: string): Promise<User[]> {
    const users = await this.userRepository.searchUsers(userName);
    return users;
  }
}
