import { User } from "src/domain/models/user.model";
import { ApiProperty } from "@nestjs/swagger";
//le DTO de requete du register,
export class UserDTO {
  @ApiProperty()
  name: string;

  @ApiProperty()
  password: string;

  @ApiProperty()
  email: string;

  static mapToModel({ name, email, password }: UserDTO) {
    return new User(name, email, password);
  }
}

//le DTO de requete du login
export class credentialsDTO {
  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;
}

//le DTO de reponse apres le requete de register
export interface ApiRgisterDTO {
  token: string;
  name: string;
}

//le DTO de reponse apres le requete de login
export interface ApiLoginDTO {
  token: string;
  name: string;
}

export interface ApiStatsUsersDTO {
  totalUsers: string;
  percentageAddedUsers: string;
}

export interface ApiUsersDeleteDTO {
  data: number[];
  message: string;
}
