import * as jwt from "jsonwebtoken";

export const generateToken = (payload: any) => {
  const secretKey = "your_secret_key";

  const options: jwt.SignOptions = {
    expiresIn: "1h",
    algorithm: "HS256"
  };

  const token = jwt.sign(payload, secretKey, options);
  return token;
};
