import { Module, forwardRef } from "@nestjs/common";

import { UsersController } from "./user.controller";
import { UserEntity } from "./user.entity";
import { UserRepository } from "src/adapters/dao/postgres/user.repository";
import { DatabaseModule } from "src/adapters/dao/postgres/db.module";
import { UserMediaEntity } from "../joins/userMedia.entity";
import { UserAlbumEntity } from "../joins/userAlbum.entity";

const usersRepositoryConf = {
  provide: "USERS_REPOSITORY",
  useValue: UserEntity
};

const userMediasRepositoryConf = {
  provide: "USER_MEDIA_REPOSITORY",
  useValue: UserMediaEntity
};

const userAlbumsRepositoryConf = {
  provide: "USER_ALBUM_REPOSITORY",
  useValue: UserAlbumEntity
};

@Module({
  imports: [forwardRef(() => DatabaseModule)],
  controllers: [UsersController],
  providers: [
    usersRepositoryConf,
    userMediasRepositoryConf,
    userAlbumsRepositoryConf,
    UserRepository
  ],
  exports: [UserRepository]
})
export class UserModule {}
