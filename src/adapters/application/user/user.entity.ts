import {
  Table,
  Column,
  Model,
  DataType,
  BelongsToMany
} from "sequelize-typescript";
import { UserMediaEntity } from "../joins/userMedia.entity";
import { UserAlbumEntity } from "../joins/userAlbum.entity";
import { MediaEntity } from "../media/media.entity";
import { AlbumEntity } from "../album/album.entity";

@Table({ tableName: "users" })
export class UserEntity extends Model {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true
  })
  id: number;
  @Column({
    type: DataType.STRING
  })
  name: string;
  @Column({
    type: DataType.STRING
  })
  password: string;
  @Column({
    type: DataType.STRING,
    allowNull: false
  })
  email: string;

  @BelongsToMany(() => MediaEntity, () => UserMediaEntity)
  medias: MediaEntity[];
  @BelongsToMany(() => AlbumEntity, () => UserAlbumEntity)
  albums: AlbumEntity[];
}
