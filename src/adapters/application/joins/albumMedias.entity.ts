import {
  Table,
  Column,
  Model,
  DataType,
  ForeignKey
} from "sequelize-typescript";
import { AlbumMedias } from "src/domain/models/albumMedias.model";
import { MediaEntity } from "../media/media.entity";
import { AlbumEntity } from "../album/album.entity";

@Table({ tableName: "albumMedias" })
export class AlbumMediasEntity extends Model<AlbumMedias> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true
  })
  id: number;
  @ForeignKey(() => MediaEntity)
  @Column({
    type: DataType.INTEGER,
    unique: false
  })
  mediaID: number;
  @ForeignKey(() => AlbumEntity)
  @Column({
    type: DataType.INTEGER,
    unique: false
  })
  albumID: number;
}
