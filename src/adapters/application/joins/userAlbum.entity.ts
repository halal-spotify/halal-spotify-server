import {
  Table,
  Column,
  Model,
  DataType,
  ForeignKey
} from "sequelize-typescript";
import { UserAlbum } from "src/domain/models/userAlbum.model";
import { UserEntity } from "../user/user.entity";
import { AlbumEntity } from "../album/album.entity";

@Table({ tableName: "userAlbums" })
export class UserAlbumEntity extends Model<UserAlbum> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true
  })
  id: number;
  @ForeignKey(() => UserEntity)
  @Column({
    type: DataType.INTEGER,
    unique: false
  })
  userID: number;
  @ForeignKey(() => AlbumEntity)
  @Column({
    type: DataType.INTEGER,
    unique: false
  })
  albumID: number;
}
