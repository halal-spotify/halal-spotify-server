import {
  Table,
  Column,
  Model,
  DataType,
  ForeignKey
} from "sequelize-typescript";
import { UserMedia } from "src/domain/models/userMedia.model";
import { MediaEntity } from "../media/media.entity";
import { UserEntity } from "../user/user.entity";

@Table({ tableName: "userMedias" })
export class UserMediaEntity extends Model<UserMedia> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true
  })
  id: number;
  @ForeignKey(() => UserEntity)
  @Column({
    type: DataType.INTEGER,
    unique: false
  })
  userID: number;
  @ForeignKey(() => MediaEntity)
  @Column({
    type: DataType.INTEGER,
    unique: false
  })
  mediaID: number;
}
