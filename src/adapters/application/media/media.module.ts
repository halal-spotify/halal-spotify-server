import { Module, forwardRef } from "@nestjs/common";

import { MediaController } from "./media.controller";
import { MediaEntity } from "./media.entity";
import { MediaRepository } from "src/adapters/dao/postgres/media.repository";
import { UserMediaEntity } from "../joins/userMedia.entity";
import { UserModule } from "../user/user.module";
import { DatabaseModule } from "src/adapters/dao/postgres/db.module";

const mediaRepositoryConf = {
  provide: "MEDIA_REPOSITORY",
  useValue: MediaEntity
};
const userMediaRepositoryConf = {
  provide: "USER_MEDIA_REPOSITORY",
  useValue: UserMediaEntity
};
const albumMediaRepositoryConf = {
  provide: "ALBUM_MEDIA_REPOSITORY",
  useValue: UserMediaEntity
};

@Module({
  imports: [forwardRef(() => UserModule), forwardRef(() => DatabaseModule)],
  controllers: [MediaController],
  providers: [
    mediaRepositoryConf,
    userMediaRepositoryConf,
    albumMediaRepositoryConf,
    MediaRepository
  ],
  exports: [MediaRepository]
})
export class MediaModule {}
