import { ApiProperty } from "@nestjs/swagger";

//le DTO de requete de upload un media
export class ApiUploadMediaReqDTO {
  @ApiProperty()
  mediaName: string;
  @ApiProperty()
  uid: number;
}

export class ApiUpdateMediaReqDTO {
  @ApiProperty()
  mediaName?: string;
  @ApiProperty()
  uid?: number;
}

//le DTO de reponse apres le requete de upload
export interface ApiUploadMediaDTO {
  medias: any[];
}

//le DTO de respose apres la supression d'un media
export interface ApiDeleteMediaDTO {
  message: string;
  data: number;
}

//le DTO de reponse apres la requete des stats
export interface ApiStatsMedisDTO {
  totalMedias: string;
  percentageAddedMedias: string;
}
