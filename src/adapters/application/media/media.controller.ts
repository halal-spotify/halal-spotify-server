import {
  Controller,
  Post,
  UseInterceptors,
  Body,
  UploadedFiles,
  Get,
  Param,
  HttpException,
  HttpStatus,
  Delete,
  Res,
  Put,
  Inject
} from "@nestjs/common";
import { FileFieldsInterceptor } from "@nestjs/platform-express";
import * as fs from "fs";
import {
  ApiTags,
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiOkResponse
} from "@nestjs/swagger";

import { MediaRepository } from "src/adapters/dao/postgres/media.repository";
import { Media } from "src/domain/models/media.model";
import {
  ApiDeleteMediaDTO,
  ApiStatsMedisDTO,
  ApiUpdateMediaReqDTO,
  ApiUploadMediaDTO,
  ApiUploadMediaReqDTO
} from "./media.dto";
import { Response } from "express";
import { UserRepository } from "src/adapters/dao/postgres/user.repository";
import {
  convertMediaToWav,
  deleteFilesFromStorage,
  getStorageConfig
} from "./utils.media";
import { join } from "path";
import { Sequelize } from "sequelize-typescript";

@ApiTags("media")
@Controller("media")
export class MediaController {
  constructor(
    private readonly mediaRepository: MediaRepository,
    private readonly userRepository: UserRepository,
    @Inject("SEQUELIZE")
    private readonly sequelize: Sequelize
  ) {}

  @Post("upload")
  @ApiBadRequestResponse({ description: "Media file not provided" })
  @ApiNotFoundResponse({ description: "User not found" })
  @ApiOkResponse({ description: "Media uploaded successfully" })
  @UseInterceptors(
    FileFieldsInterceptor(
      [{ name: "mediaFile" }, { name: "imgFile" }],
      getStorageConfig()
    )
  )
  async uploadMedia(
    @UploadedFiles() files: any,
    @Body() { mediaName, uid }: ApiUploadMediaReqDTO
  ): Promise<ApiUploadMediaDTO> {
    const mediaFilePath = files["mediaFile"]
      ? files["mediaFile"][0].path
      : null;
    const imageFilePath = files["imgFile"] ? files["imgFile"][0].path : null;

    if (!files["mediaFile"]) {
      deleteFilesFromStorage(mediaFilePath, imageFilePath);

      throw new HttpException(
        "Media file not provided",
        HttpStatus.BAD_GATEWAY
      );
    }

    //check if the user exists on the db
    const user = await this.userRepository.getUserById(uid);
    if (!user) {
      deleteFilesFromStorage(mediaFilePath, imageFilePath);
      throw new HttpException("User not found", 404);
    }

    const convertedFilePath = (await convertMediaToWav(
      mediaFilePath
    )) as string;

    const media = new Media();
    media.filePath = convertedFilePath;
    media.imagePath = imageFilePath;
    media.name = mediaName;

    await this.mediaRepository.createMedia(media, uid);
    const medias = await this.mediaRepository.getAllMedias();

    return {
      medias
    };
  }

  @Put("/:mediaID")
  @ApiBadRequestResponse({ description: "Media file not provided" })
  @ApiNotFoundResponse({ description: "User not found" })
  @ApiOkResponse({ description: "Media uploaded successfully" })
  @UseInterceptors(
    FileFieldsInterceptor(
      [{ name: "mediaFile" }, { name: "imgFile" }],
      getStorageConfig()
    )
  )
  async updateMedia(
    @Body() { mediaName, uid }: ApiUpdateMediaReqDTO,
    @Param("mediaID") mediaID: string,
    @UploadedFiles() files?: any
  ): Promise<{ data: Media; message: string }> {
    const prevMedia = await this.mediaRepository.getSingleMedia(mediaID);
    if (!prevMedia) {
      throw new HttpException("Track not found", 404);
    }

    // Cleaning old data if exists
    if (files["mediaFile"]) {
      deleteFilesFromStorage(prevMedia.filePath, null);
    }
    if (files["imgFile"]) {
      deleteFilesFromStorage(null, prevMedia.imagePath);
    }

    //check if the user exists on the db
    if (uid) {
      const user = await this.userRepository.getUserById(uid);
      if (!user) {
        if (files["mediaFile"]) {
          deleteFilesFromStorage(files["mediaFile"][0].path, null);
        }
        if (files["imgFile"]) {
          deleteFilesFromStorage(null, files["imgFile"][0].path);
        }
        throw new HttpException("User not found", 404);
      }
    }

    let convertedFilePath = null;
    if (files["mediaFile"])
      convertedFilePath = (await convertMediaToWav(
        files["mediaFile"][0].path
      )) as string;

    const media = new Media();
    media.filePath = convertedFilePath || prevMedia.filePath;
    media.imagePath = prevMedia.imagePath;
    if (files["imgFile"]) media.imagePath = files["imgFile"][0].path;
    media.name = mediaName || prevMedia.name;

    await this.mediaRepository.updateMedia(media, mediaID);
    if (prevMedia.users[0]?.id !== uid)
      await this.mediaRepository.updateUserMedia(
        prevMedia.users[0]?.id || null,
        uid,
        mediaID
      );

    const newMedia = await this.mediaRepository.getSingleMedia(mediaID);
    return {
      data: newMedia,
      message: "Track updated successfully!"
    };
  }

  @Get("search/:mediaName")
  async getMediaByName(
    @Param("mediaName") mediaName: string
  ): Promise<Media[]> {
    const media = await this.mediaRepository.searchMedia(mediaName);
    return media;
  }

  @Get("user/:userID")
  async getMediaByUser(@Param("userID") userID: number): Promise<Media[]> {
    const media = await this.mediaRepository.userMedias(userID);
    return media;
  }

  @Get("")
  async getAllMedias(): Promise<any[]> {
    const medias = await this.mediaRepository.getAllMedias();
    return medias;
  }

  @Get("tenmedia")
  async getTenMedias(): Promise<any[]> {
    const medias = await this.mediaRepository.getAllMedias(10);
    return medias;
  }

  @Get("single/:mediaID")
  async getSingleMedia(@Param("mediaID") mediaID: string): Promise<Media> {
    if (mediaID.length <= 0)
      throw new HttpException("MediaID is required", HttpStatus.NOT_FOUND);

    const media = await this.mediaRepository.getSingleMedia(mediaID);
    if (!media)
      throw new HttpException("Media not found", HttpStatus.NOT_FOUND);
    return media;
  }

  @Get("cover/:mediaID")
  async getSingleCoverMedia(
    @Param("mediaID") mediaID: string,
    @Res() response
  ): Promise<void> {
    if (mediaID.length <= 0)
      throw new HttpException("MediaID is required", HttpStatus.NOT_FOUND);

    const media = await this.mediaRepository.getSingleMedia(mediaID);
    const mediaFilePath = fs.createReadStream(
      join(process.cwd(), media.imagePath)
    );

    mediaFilePath.pipe(response);
  }

  @Get("file/:mediaID")
  async getSingleFileMedia(
    @Param("mediaID") mediaID: string,
    @Res() response
  ): Promise<void> {
    if (mediaID.length <= 0)
      throw new HttpException("MediaID is required", HttpStatus.NOT_FOUND);

    const media = await this.mediaRepository.getSingleMedia(mediaID);
    await this.mediaRepository.addMediaCount(mediaID);
    const mediaFilePath = fs.readFileSync(join(process.cwd(), media.filePath));

    response.attachment(media.filePath);
    response.send(mediaFilePath);
  }

  @Delete(":mediaID")
  async deleteMediaById(
    @Param("mediaID") mediaID: number
  ): Promise<ApiDeleteMediaDTO> {
    const media = await this.mediaRepository.getMedia(mediaID);
    if (!media) {
      throw new HttpException("Media not found", HttpStatus.NOT_FOUND);
    }

    const mediaFilePath = media.filePath;
    const mediaImgPath = media.imagePath;

    const transaction = await this.sequelize.transaction();
    try {
      await this.mediaRepository.deleteUserMedia(mediaID, transaction);
      await this.mediaRepository.deleteAlbumMedia(mediaID, transaction);
      await this.mediaRepository.deleteMedia(mediaID, transaction);
      deleteFilesFromStorage(mediaFilePath, mediaImgPath);

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw new HttpException(
        "Error while deleting the Track",
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }

    return {
      message: "Track deleted succesfully!",
      data: mediaID
    };
  }

  @Get("stream/:mediaID")
  async streamMediaById(
    @Param("mediaID") mediaID: number,
    @Res() response: Response
  ): Promise<void> {
    const media = await this.mediaRepository.getMedia(mediaID);
    if (!media) {
      throw new HttpException("Media not found", HttpStatus.NOT_FOUND);
    }

    await this.mediaRepository.addMediaCount(mediaID as any);

    const mediaFilePath = media.filePath;
    const stat = fs.statSync(mediaFilePath);
    const fileSize = stat.size;
    const headers = {
      "Content-Length": fileSize,
      "Content-Type": "audio/mp3",
      "Accept-ranges": "bytes",
      "Content-Range": "bytes 0-" + fileSize + "/" + fileSize
    };

    response.writeHead(200, headers);
    fs.createReadStream(mediaFilePath).pipe(response);
  }

  @Get("stats")
  async getMediaStats(): Promise<ApiStatsMedisDTO> {
    const totalMedias = await this.mediaRepository.getMediasStats();

    const date = new Date();
    const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    const totalMediasThisMonth = await this.mediaRepository.getMediasStats(
      firstDay.toISOString(),
      lastDay.toISOString()
    );

    const firstDayLastMonth = new Date(
      date.getFullYear(),
      date.getMonth() - 1,
      1
    );
    const lastDayLastMonth = new Date(date.getFullYear(), date.getMonth(), 0);
    const totalMediasLastMonth = await this.mediaRepository.getMediasStats(
      firstDayLastMonth.toISOString(),
      lastDayLastMonth.toISOString()
    );

    let percentageAddedMedias = "0.00%";
    if (parseInt(totalMedias) !== 0) {
      const count =
        ((parseInt(totalMediasThisMonth) - parseInt(totalMediasLastMonth)) *
          100) /
        parseInt(totalMedias);
      if (count >= 0) percentageAddedMedias = count.toFixed(2) + "%";
    }

    return {
      totalMedias,
      percentageAddedMedias
    };
  }

  @Get("listenStats")
  async getMediaListenStats(): Promise<{
    totalListens: string;
    percentageTotalListens: string;
  }> {
    const totalListens =
      (await this.mediaRepository.getMediasListenStats()) || "0";

    const date = new Date();
    const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    const totalListensThisMonth =
      await this.mediaRepository.getMediasListenStats(
        firstDay.toISOString(),
        lastDay.toISOString()
      );

    const firstDayLastMonth = new Date(
      date.getFullYear(),
      date.getMonth() - 1,
      1
    );
    const lastDayLastMonth = new Date(date.getFullYear(), date.getMonth(), 0);
    const totalListensLastMonth =
      await this.mediaRepository.getMediasListenStats(
        firstDayLastMonth.toISOString(),
        lastDayLastMonth.toISOString()
      );

    let percentageTotalListens = "0.00%";
    if (parseInt(totalListens) !== 0) {
      const count =
        ((parseInt(totalListensThisMonth) - parseInt(totalListensLastMonth)) *
          100) /
        parseInt(totalListens);
      if (count >= 0) percentageTotalListens = count.toFixed(2) + "%";
    }

    return {
      totalListens,
      percentageTotalListens
    };
  }
}
