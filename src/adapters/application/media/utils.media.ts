import { diskStorage } from "multer";
import { extname } from "path";
import * as ffmpeg from "fluent-ffmpeg";
import { path as ffmpegPath } from "@ffmpeg-installer/ffmpeg";
import * as fs from "fs";

ffmpeg.setFfmpegPath(ffmpegPath);

export const convertMediaToWav = (filePath: string) => {
  return new Promise((resolve, reject) => {
    const outputFilePath = `${filePath}.m4a`;

    ffmpeg(filePath)
      .toFormat("mp4")
      .on("end", () => {
        resolve(outputFilePath);
        fs.unlink(filePath, (err) => {
          if (err) {
            console.error(err);
            return;
          }
        });
      })
      .on("error", (err) => {
        reject(err);
      })
      .save(outputFilePath);
  });
};

export const getStorageConfig = () => {
  return {
    storage: diskStorage({
      destination: "./src/adapters/dao/uploads",
      filename: (req, file, callback) => {
        if (!file) {
          return;
        }
        const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
        const extension = extname(file.originalname);
        callback(null, file.fieldname + "-" + uniqueSuffix + extension);
      }
    })
  };
};

export const deleteFilesFromStorage = (
  mediaFilePath: string,
  mediaImgPath: string
) => {
  // Delete media file
  if (mediaFilePath)
    fs.unlink(mediaFilePath, (err) => {
      if (err) {
        console.error(err);
      }
    });

  // Delete media image file
  if (mediaImgPath)
    fs.unlink(mediaImgPath, (err) => {
      if (err) {
        console.error(err);
      }
    });
};
