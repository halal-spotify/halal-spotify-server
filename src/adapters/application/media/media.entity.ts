import {
  Table,
  Column,
  Model,
  DataType,
  BelongsToMany
} from "sequelize-typescript";
import { Media } from "src/domain/models/media.model";
import { UserMediaEntity } from "../joins/userMedia.entity";
import { AlbumMediasEntity } from "../joins/albumMedias.entity";
import { UserEntity } from "../user/user.entity";
import { AlbumEntity } from "../album/album.entity";

@Table({ tableName: "medias" })
export class MediaEntity extends Model<Media> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true
  })
  id: number;
  @Column({
    type: DataType.STRING
  })
  name: string;
  @Column({
    type: DataType.STRING
  })
  imagePath: string;
  @Column({
    type: DataType.STRING
  })
  filePath: string;
  @Column({
    type: DataType.INTEGER
  })
  listensCount: number;

  @BelongsToMany(() => UserEntity, () => UserMediaEntity)
  users: UserEntity[];
  @BelongsToMany(() => AlbumEntity, () => AlbumMediasEntity)
  albums: AlbumEntity[];
}
