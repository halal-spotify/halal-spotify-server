import {
  Table,
  Column,
  Model,
  DataType,
  BelongsToMany
} from "sequelize-typescript";
import { Album } from "src/domain/models/album.model";
import { AlbumMediasEntity } from "../joins/albumMedias.entity";
import { UserAlbumEntity } from "../joins/userAlbum.entity";
import { UserEntity } from "../user/user.entity";
import { MediaEntity } from "../media/media.entity";

@Table({ tableName: "albums" })
export class AlbumEntity extends Model<Album> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true
  })
  id: number;
  @Column({
    type: DataType.STRING
  })
  name: string;
  @Column({
    type: DataType.STRING
  })
  coverPath: string;

  @BelongsToMany(() => MediaEntity, () => AlbumMediasEntity)
  medias: MediaEntity[];
  @BelongsToMany(() => UserEntity, () => UserAlbumEntity)
  users: UserEntity[];
}
