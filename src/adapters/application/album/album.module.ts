import { Module, forwardRef } from "@nestjs/common";

import { AlbumsController } from "./album.controller";
import { AlbumEntity } from "./album.entity";
import { AlbumRepository } from "src/adapters/dao/postgres/album.repository";
import { UserAlbumEntity } from "../joins/userAlbum.entity";
import { AlbumMediasEntity } from "../joins/albumMedias.entity";
import { DatabaseModule } from "src/adapters/dao/postgres/db.module";
import { UserModule } from "../user/user.module";

const albumsRepositoryConf = {
  provide: "ALBUMS_REPOSITORY",
  useValue: AlbumEntity
};
const userAlbumRepositoryConf = {
  provide: "USER_ALBUMS_REPOSITORY",
  useValue: UserAlbumEntity
};

const albumMediasRepositoryConf = {
  provide: "ALBUM_MEDIAS_REPOSITORY",
  useValue: AlbumMediasEntity
};

@Module({
  imports: [forwardRef(() => UserModule), forwardRef(() => DatabaseModule)],
  controllers: [AlbumsController],
  providers: [
    albumsRepositoryConf,
    userAlbumRepositoryConf,
    albumMediasRepositoryConf,
    AlbumRepository
  ],
  exports: [AlbumRepository]
})
export class AlbumModule {}
