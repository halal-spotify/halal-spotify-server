export class AlbumDTO {
  id: number;
  name: string;
  coverPath: string;
}

export interface newAlbumDTO {
  name: string;
  mediaIDs: number[];
  userID: number;
}

export class UpdateAlbumDTO {
  id: number;
  name?: string;
  coverPath?: string;
  mediaIDs?: number[];
  userID?: string;
}

export interface ApiCreateAlbumDTO {
  message: string;
  data: AlbumDTO[];
}

export interface ApiAlbumDeleteDTO {
  message: string;
  data: number[];
}

export class ApiUpdatedAlbumDTO {
  message: string;
  data: AlbumDTO;
}

export class ApiStatsAlbumsDTO {
  totalAlbums: string;
  percentageAddedAlbums: string;
}
