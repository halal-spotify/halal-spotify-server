import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Get,
  Post,
  Inject,
  UseInterceptors,
  UploadedFiles,
  Delete,
  Put,
  Param,
  Res
} from "@nestjs/common";
import * as fs from "fs";
import { join } from "path";

import {
  AlbumDTO,
  ApiAlbumDeleteDTO,
  ApiCreateAlbumDTO,
  ApiStatsAlbumsDTO,
  ApiUpdatedAlbumDTO,
  UpdateAlbumDTO,
  newAlbumDTO
} from "./album.dto";
import { AlbumRepository } from "../../dao/postgres/album.repository";
import { Album } from "src/domain/models/album.model";
import { Sequelize } from "sequelize-typescript";
import { FileFieldsInterceptor } from "@nestjs/platform-express";
import { diskStorage } from "multer";
import * as path from "path";
import { ApiTags } from "@nestjs/swagger";
import { deleteFilesFromStorage } from "../media/utils.media";
import { UserRepository } from "src/adapters/dao/postgres/user.repository";
import { UserAlbumEntity } from "../joins/userAlbum.entity";
import { AlbumEntity } from "./album.entity";

@ApiTags("albums")
@Controller("albums")
export class AlbumsController {
  constructor(
    private readonly albumRepository: AlbumRepository,
    private readonly userRepository: UserRepository,
    @Inject("SEQUELIZE")
    private sequelize: Sequelize
  ) {}

  @Get("")
  async getAlbums(): Promise<AlbumDTO[]> {
    const albums = await this.albumRepository.getAlbums();

    return albums;
  }

  @Get("tenalbums")
  async getTenAlbums(): Promise<AlbumDTO[]> {
    const albums = await this.albumRepository.getAlbums(10);

    return albums;
  }

  @Get("cover/:albumID")
  async getAlbumCover(
    @Param("albumID") albumID: string,
    @Res() response
  ): Promise<void> {
    if (albumID.length <= 0)
      throw new HttpException("MediaID is required", HttpStatus.NOT_FOUND);

    const album = await this.albumRepository.getSingleAlbum(parseInt(albumID));
    const mediaFilePath = fs.createReadStream(
      join(process.cwd(), album.coverPath)
    );

    mediaFilePath.pipe(response);
  }

  @Get("album/:id")
  async getSingleAlbum(@Param("id") id: number): Promise<AlbumDTO> {
    const album = await this.albumRepository.getSingleAlbum(id);
    if (!album) {
      throw new HttpException("Album does not exist", HttpStatus.UNAUTHORIZED);
    }

    return album;
  }

  @Post("")
  @UseInterceptors(
    FileFieldsInterceptor([{ name: "coverPath" }], {
      storage: diskStorage({
        destination: "./src/adapters/dao/uploads/albumCovers",
        filename: (req, file, callback) => {
          const uniqueSuffix =
            Date.now() + "-" + Math.round(Math.random() * 1e9);
          const extension = path.extname(file.originalname);
          callback(null, file.fieldname + "-" + uniqueSuffix + extension);
        }
      })
    })
  )
  async createAlbum(
    @UploadedFiles() albumCover: any,
    @Body() { name, mediaIDs, userID }: newAlbumDTO
  ): Promise<ApiCreateAlbumDTO> {
    const transaction = await this.sequelize.transaction();
    try {
      const newAlbum = new Album();
      newAlbum.name = name;
      newAlbum.coverPath = albumCover["coverPath"][0].path;

      const newAlbumEntity = await this.albumRepository.createAlbum(
        newAlbum,
        userID,
        transaction
      );

      if (typeof mediaIDs === "string") {
        mediaIDs = [parseInt(mediaIDs)];
      }
      for (const mediaID of mediaIDs) {
        await this.albumRepository.addMediaToAlbum(
          mediaID,
          newAlbumEntity.id,
          transaction
        );
      }

      await transaction.commit();
    } catch (err) {
      console.log(err);
      await transaction.rollback();

      throw new HttpException(
        "Error while creating album",
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }

    const albums = await this.albumRepository.getAlbums();

    return {
      data: albums,
      message: "Album created successfully!"
    };
  }

  @Delete("")
  async deleteAlbum(
    @Body() { albumIDs }: { albumIDs: number[] }
  ): Promise<ApiAlbumDeleteDTO> {
    if (!albumIDs) {
      throw new HttpException("AlbumID is required", HttpStatus.BAD_REQUEST);
    }

    const transaction = await this.sequelize.transaction();
    try {
      await this.albumRepository.deleteAlbums(albumIDs);
      await this.albumRepository.deleteUserAlbums(albumIDs);
      await this.albumRepository.deleteAlbumMedias(albumIDs);

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw new HttpException(
        "Error while deleting albums",
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }

    return {
      data: albumIDs,
      message: "Album(s) deleted successfully!"
    };
  }

  @Put("")
  @UseInterceptors(
    FileFieldsInterceptor([{ name: "coverPath" }], {
      storage: diskStorage({
        destination: "./src/adapters/dao/uploads/albumCovers",
        filename: (req, file, callback) => {
          const uniqueSuffix =
            Date.now() + "-" + Math.round(Math.random() * 1e9);
          const extension = path.extname(file.originalname);
          callback(null, file.fieldname + "-" + uniqueSuffix + extension);
        }
      })
    })
  )
  async updateAlbum(
    @Body() newAlbumData: UpdateAlbumDTO,
    @UploadedFiles() albumCover?: any
  ): Promise<ApiUpdatedAlbumDTO> {
    const prevAlbumData = await this.albumRepository.getSingleAlbum(
      newAlbumData.id
    );
    if (!prevAlbumData) {
      throw new HttpException(
        "You are trying to update an album that does not exist",
        HttpStatus.BAD_REQUEST
      );
    }

    // Cleaning old data if exists
    if (albumCover["coverPath"]) {
      deleteFilesFromStorage(prevAlbumData.coverPath, null);
    }

    //check if the user exists on the db
    if (newAlbumData.userID) {
      const user = await this.userRepository.getSingleUser(
        parseInt(newAlbumData.userID)
      );
      if (!user) {
        if (albumCover["coverPath"]) {
          deleteFilesFromStorage(albumCover["coverPath"][0].path, null);
        }
        throw new HttpException("User not found", 404);
      }
    }

    const transaction = await this.sequelize.transaction();
    try {
      for (const media of prevAlbumData.medias) {
        if (newAlbumData.mediaIDs.includes(media.id))
          await this.albumRepository.deleteMediaFromAlbum(
            media.id,
            prevAlbumData.id,
            transaction
          );
      }

      for (const mediaID of newAlbumData.mediaIDs) {
        if (
          !prevAlbumData.medias.find(
            (media) => media.id.toString() === mediaID.toString()
          )
        )
          await this.albumRepository.addMediaToAlbum(
            mediaID,
            prevAlbumData.id,
            transaction
          );
      }

      const updatedAlbumData = new Album();
      updatedAlbumData.id = newAlbumData.id;
      updatedAlbumData.name = newAlbumData.name || prevAlbumData.name;
      updatedAlbumData.coverPath = prevAlbumData.coverPath;
      if (albumCover["coverPath"])
        updatedAlbumData.coverPath = albumCover["coverPath"][0].path;

      await this.albumRepository.updateAlbum(updatedAlbumData, transaction);
      if (prevAlbumData.users[0]?.id !== parseInt(newAlbumData.userID))
        await this.albumRepository.updateUserMedia(
          prevAlbumData.id,
          prevAlbumData.users[0]?.id || null,
          newAlbumData.userID,
          transaction
        );

      await transaction.commit();
    } catch (err) {
      console.log(err);
      await transaction.rollback();

      throw new HttpException(
        "Error while updating album",
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }

    const newAlbumEntity = await this.albumRepository.getSingleAlbum(
      newAlbumData.id
    );
    return {
      message: "Album updated succesfully!",
      data: newAlbumEntity
    };
  }

  @Get("stats")
  async getMediaStats(): Promise<ApiStatsAlbumsDTO> {
    const totalAlbums = await this.albumRepository.getAlbumsStats();

    const date = new Date();
    const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    const totalAlbumsThisMonth = await this.albumRepository.getAlbumsStats(
      firstDay.toISOString(),
      lastDay.toISOString()
    );

    const firstDayLastMonth = new Date(
      date.getFullYear(),
      date.getMonth() - 1,
      1
    );
    const lastDayLastMonth = new Date(date.getFullYear(), date.getMonth(), 0);
    const totalAlbumsLastMonth = await this.albumRepository.getAlbumsStats(
      firstDayLastMonth.toISOString(),
      lastDayLastMonth.toISOString()
    );

    let percentageAddedAlbums = "0.00%";
    if (parseInt(totalAlbums) !== 0) {
      const count =
        ((parseInt(totalAlbumsThisMonth) - parseInt(totalAlbumsLastMonth)) *
          100) /
        parseInt(totalAlbums);
      if (count >= 0) percentageAddedAlbums = count.toFixed(2) + "%";
    }

    return {
      totalAlbums,
      percentageAddedAlbums
    };
  }

  @Get("search/:albumName")
  async getAlbumByName(
    @Param("albumName") albumName: string
  ): Promise<Album[]> {
    const albums = await this.albumRepository.searchAlbum(albumName);
    return albums;
  }

  @Get("user/:userID")
  async getAlbumByUser(
    @Param("userID") userID: number
  ): Promise<AlbumEntity[] | UserAlbumEntity[]> {
    const albums = await this.albumRepository.userAlbums(userID);
    return albums;
  }
}
