import { Inject, Injectable } from "@nestjs/common";

import { AlbumPort } from "src/domain/ports/album.port";
import { AlbumEntity } from "../../application/album/album.entity";
import { Album } from "src/domain/models/album.model";
import { UserAlbumEntity } from "src/adapters/application/joins/userAlbum.entity";
import { AlbumMediasEntity } from "src/adapters/application/joins/albumMedias.entity";
import { Op, Transaction } from "sequelize";
import { UserEntity } from "src/adapters/application/user/user.entity";
import { MediaEntity } from "src/adapters/application/media/media.entity";

@Injectable()
export class AlbumRepository implements AlbumPort {
  constructor(
    @Inject("ALBUMS_REPOSITORY")
    private albumPersistence: typeof AlbumEntity,
    @Inject("USER_ALBUMS_REPOSITORY")
    private userAlbumPersistence: typeof UserAlbumEntity,
    @Inject("ALBUM_MEDIAS_REPOSITORY")
    private albumMediaPersistence: typeof AlbumMediasEntity
  ) {}

  getAlbums(limit?: number): Promise<AlbumEntity[]> {
    const albums = this.albumPersistence.findAll({
      include: [
        {
          identifier: "medias",
          model: MediaEntity,
          include: [
            {
              identifier: "users",
              model: UserEntity,
              attributes: ["name", "email", "id"]
            }
          ]
        },
        {
          identifier: "users",
          model: UserEntity,
          attributes: ["name", "email", "id"]
        }
      ],
      limit: limit
    });
    return albums;
  }

  getSingleAlbum(albumID: number): Promise<AlbumEntity> {
    if (!albumID) {
      throw new Error("Album ID is required");
    }
    const album = this.albumPersistence.findByPk(albumID, {
      include: [
        {
          identifier: "medias",
          model: MediaEntity,
          include: [
            {
              identifier: "users",
              model: UserEntity,
              attributes: ["name", "email", "id"]
            }
          ]
        },
        {
          identifier: "users",
          model: UserEntity,
          attributes: ["name", "email", "id"]
        }
      ]
    });
    return album;
  }

  async createAlbum(
    album: Album,
    userID: number,
    transaction: Transaction
  ): Promise<AlbumEntity> {
    const newAlbum = await this.albumPersistence.create(album, { transaction });
    await this.userAlbumPersistence.create(
      {
        albumID: newAlbum.id,
        userID: userID
      },
      { transaction }
    );

    return newAlbum;
  }

  addMediaToAlbum(mediaID: number, albumID: number, transaction: Transaction) {
    return this.albumMediaPersistence.create(
      {
        albumID: albumID,
        mediaID: mediaID
      },
      { transaction }
    );
  }
  deleteMediaFromAlbum(
    mediaID: number,
    albumID: number,
    transaction: Transaction
  ) {
    return this.albumMediaPersistence.destroy({
      where: {
        albumID: albumID,
        mediaID: mediaID
      },
      transaction: transaction
    });
  }

  async deleteAlbums(albumIDs: number[]): Promise<void> {
    albumIDs.forEach(async (albumID) => {
      await this.albumPersistence.destroy({
        where: {
          id: albumID
        }
      });
    });
  }
  async deleteUserAlbums(albumIDs: number[]): Promise<void> {
    albumIDs.forEach(async (albumID) => {
      await this.userAlbumPersistence.destroy({
        where: {
          albumID: albumID
        }
      });
    });
  }
  async deleteAlbumMedias(albumIDs: number[]): Promise<void> {
    albumIDs.forEach(async (albumID) => {
      await this.albumMediaPersistence.destroy({
        where: {
          albumID: albumID
        }
      });
    });
  }

  async updateAlbum(album: Album, transaction: Transaction): Promise<void> {
    await this.albumPersistence.update(album, {
      where: {
        id: album.id
      },
      transaction: transaction
    });
  }

  async updateUserMedia(
    albumID: number,
    oldUserID: number,
    newUserID: string,
    transaction: Transaction
  ): Promise<void> {
    if (oldUserID === null) {
      await this.userAlbumPersistence.create(
        {
          albumID: albumID,
          userID: parseInt(newUserID)
        },
        { transaction }
      );
    } else {
      await this.userAlbumPersistence.update(
        {
          userID: parseInt(newUserID)
        },
        {
          where: {
            albumID: albumID,
            userID: oldUserID
          },
          transaction: transaction
        }
      );
    }
  }

  async getAlbumsStats(startDate?: string, endDate?: string): Promise<string> {
    if (startDate === undefined || endDate === undefined) {
      const count = await this.albumPersistence.count();
      return count.toString();
    }

    const count = await this.albumPersistence.count({
      where: {
        createdAt: {
          [Op.between]: [startDate, endDate]
        }
      } as any
    });
    return count.toString();
  }

  async searchAlbum(albumName: string) {
    return this.albumPersistence.findAll({
      where: { name: { [Op.iLike]: `%${albumName}%` } }
    });
  }

  async userAlbums(userID: number) {
    return this.albumPersistence.findAll({
      where: { "$medias.users.UserMediaEntity.userID$": userID },
      include: [
        {
          identifier: "medias",
          model: MediaEntity,
          include: [
            {
              identifier: "users",
              model: UserEntity,
              attributes: ["name", "email", "id"]
            }
          ]
        },
        {
          identifier: "users",
          model: UserEntity,
          attributes: ["name", "email", "id"]
        }
      ]
    });
  }
}
