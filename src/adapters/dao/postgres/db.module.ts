import { Sequelize } from "sequelize-typescript";
import { Module } from "@nestjs/common";
import { entities } from "./entities";
import * as dotenv from "dotenv";

dotenv.config();

const databaseProviders = [
  {
    provide: "SEQUELIZE",
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: "postgres",
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE
      });
      sequelize.addModels(entities);
      await sequelize.sync();
      return sequelize;
    }
  }
];

@Module({
  providers: [...databaseProviders],
  exports: [...databaseProviders]
})
export class DatabaseModule {}
