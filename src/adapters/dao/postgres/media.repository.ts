import { Injectable, Inject } from "@nestjs/common";
import { Op, Transaction } from "sequelize";

import { MediaPort } from "src/domain/ports/media.port";
import { MediaEntity } from "src/adapters/application/media/media.entity";
import { Media } from "src/domain/models/media.model";
import { UserMediaEntity } from "src/adapters/application/joins/userMedia.entity";
import { UserEntity } from "src/adapters/application/user/user.entity";
import { AlbumMediasEntity } from "src/adapters/application/joins/albumMedias.entity";

@Injectable()
export class MediaRepository implements MediaPort {
  constructor(
    @Inject("MEDIA_REPOSITORY")
    private mediaPersistence: typeof MediaEntity,
    @Inject("USER_MEDIA_REPOSITORY")
    private userMediaPersistence: typeof UserMediaEntity,
    @Inject("ALBUM_MEDIA_REPOSITORY")
    private albumMediaPersistence: typeof AlbumMediasEntity
  ) {}

  async createMedia(media: Media, userID: number) {
    const newMedia = await this.mediaPersistence.create(media);
    await this.userMediaPersistence.create({
      mediaID: newMedia.id,
      userID: userID
    });
  }

  async updateMedia(
    media: Media,
    mediaID: string,
    transaction?: Transaction
  ): Promise<void> {
    await this.mediaPersistence.update(media, {
      where: {
        id: mediaID
      },
      transaction: transaction
    });
  }
  async updateUserMedia(
    oldUserID: number,
    userID: number,
    mediaID: string,
    transaction?: Transaction
  ): Promise<void> {
    if (oldUserID === null) {
      await this.userMediaPersistence.create(
        {
          mediaID: parseInt(mediaID),
          userID: userID
        },
        { transaction }
      );
    } else {
      await this.userMediaPersistence.update(
        {
          userID: userID
        },
        {
          where: {
            mediaID: mediaID,
            userID: oldUserID
          },
          transaction: transaction
        }
      );
    }
  }

  async updateMediaImg(imgPath: string, mediaID: number) {
    const media = await this.mediaPersistence.findOne({
      where: { id: mediaID }
    });

    return this.mediaPersistence.update(
      { ...media, imagePath: imgPath },
      { where: { id: mediaID } }
    );
  }

  async searchMedia(mediaName: string) {
    return this.mediaPersistence.findAll({
      where: { name: { [Op.iLike]: `%${mediaName}%` } },
      include: [
        {
          identifier: "user",
          model: UserEntity,
          attributes: ["name", "email", "id"]
        }
      ]
    });
  }

  async getSingleMedia(mediaID: string) {
    return this.mediaPersistence.findByPk(parseInt(mediaID), {
      include: [
        {
          identifier: "user",
          model: UserEntity,
          attributes: ["name", "email", "id"]
        }
      ]
    });
  }

  async getMedia(mediaID: number) {
    return this.mediaPersistence.findOne({ where: { id: mediaID } });
  }

  async deleteMedia(mediaID: number, transaction: Transaction = null) {
    await this.mediaPersistence.destroy({
      where: { id: mediaID },
      transaction: transaction
    });
  }

  async deleteUserMedia(
    mediaID: number,
    transaction: Transaction
  ): Promise<number> {
    return this.userMediaPersistence.destroy({
      where: {
        mediaID: mediaID
      },
      transaction: transaction
    });
  }

  async deleteAlbumMedia(
    mediaID: number,
    transaction: Transaction
  ): Promise<number> {
    return this.albumMediaPersistence.destroy({
      where: {
        mediaID: mediaID
      },
      transaction: transaction
    });
  }

  async getAllMedias(limit?: number) {
    return this.mediaPersistence.findAll({
      include: [
        {
          model: UserEntity,
          attributes: ["name", "email", "id"]
        }
      ],
      limit: limit
    });
  }

  async getMediasStats(startDate?: string, endDate?: string): Promise<string> {
    if (startDate === undefined || endDate === undefined) {
      const count = await this.mediaPersistence.count();
      return count.toString();
    }

    const count = await this.mediaPersistence.count({
      where: {
        createdAt: {
          [Op.between]: [startDate, endDate]
        }
      } as any
    });
    return count.toString();
  }

  async getMediasListenStats(
    startDate?: string,
    endDate?: string
  ): Promise<string> {
    const sequelize = this.mediaPersistence.sequelize;

    if (startDate === undefined || endDate === undefined) {
      const total = (await this.mediaPersistence.findAll({
        attributes: [
          [sequelize.fn("sum", sequelize.col("listensCount")), "total"]
        ],
        raw: true
      })) as unknown;
      return total[0].total;
    }

    const total = (await this.mediaPersistence.findAll({
      where: {
        createdAt: {
          [Op.between]: [startDate, endDate]
        }
      } as any,
      attributes: [
        [sequelize.fn("sum", sequelize.col("listensCount")), "total"]
      ],
      raw: true
    })) as unknown;

    return total[0].total;
  }

  async addMediaCount(mediaID: string): Promise<void> {
    const media = await this.mediaPersistence.findOne({
      where: { id: mediaID }
    });
    media.listensCount += 1;
    await media.save();
  }

  async userMedias(userID: number) {
    return this.mediaPersistence.findAll({
      where: { "$users.UserMediaEntity.userID$": userID },
      include: [
        {
          identifier: "user",
          model: UserEntity,
          attributes: ["name", "email", "id"]
        }
      ]
    });
  }
}
