import { AlbumEntity } from "src/adapters/application/album/album.entity";
import { UserEntity } from "src/adapters/application/user/user.entity";
import { MediaEntity } from "src/adapters/application/media/media.entity";
import { AlbumMediasEntity } from "src/adapters/application/joins/albumMedias.entity";
import { UserAlbumEntity } from "src/adapters/application/joins/userAlbum.entity";
import { UserMediaEntity } from "src/adapters/application/joins/userMedia.entity";

export const entities = [
  UserEntity,
  AlbumEntity,
  MediaEntity,
  AlbumMediasEntity,
  UserAlbumEntity,
  UserMediaEntity
];
