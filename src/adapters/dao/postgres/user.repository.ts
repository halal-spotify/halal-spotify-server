import { Inject, Injectable } from "@nestjs/common";

import { User } from "src/domain/models/user.model";
import { UserPort } from "src/domain/ports/user.port";
import { UserEntity } from "../../application/user/user.entity";
import { Op } from "sequelize";
import { Transaction } from "sequelize/types";
import { UserMediaEntity } from "src/adapters/application/joins/userMedia.entity";
import { UserAlbumEntity } from "src/adapters/application/joins/userAlbum.entity";

@Injectable()
export class UserRepository implements UserPort {
  constructor(
    @Inject("USERS_REPOSITORY")
    private userPersistence: typeof UserEntity,
    @Inject("USER_MEDIA_REPOSITORY")
    private userMediaPersistence: typeof UserMediaEntity,
    @Inject("USER_ALBUM_REPOSITORY")
    private userAlbumPersistence: typeof UserAlbumEntity
  ) {}

  async getAllUsers(limit?: number): Promise<User[]> {
    const users = await this.userPersistence.findAll({
      attributes: {
        exclude: ["password"]
      },
      limit: limit
    });
    return users;
  }

  getUser(email: string) {
    return this.userPersistence.findOne({
      where: { email: email }
    });
  }

  getSingleUser(id: number) {
    return this.userPersistence.findByPk(id, {
      attributes: {
        exclude: ["password"]
      }
    });
  }

  updateUser(user: User, userID: number) {
    const updated = this.userPersistence.update(user, {
      where: {
        id: userID
      },
      returning: ["id", "name", "email"]
    });
    return updated;
  }

  createUser(user: User) {
    const userEntity = {
      name: user.name,
      email: user.email,
      password: user.password
    };
    return this.userPersistence.create(userEntity);
  }

  getUserById(id: number) {
    return this.userPersistence.findOne({ where: { id: id } });
  }

  async getUsersStats(startDate?: string, endDate?: string): Promise<string> {
    if (startDate === undefined || endDate === undefined) {
      const count = await this.userPersistence.count();
      return count.toString();
    }

    const count = await this.userPersistence.count({
      where: {
        createdAt: {
          [Op.between]: [startDate, endDate]
        }
      } as any
    });
    return count.toString();
  }

  async deleteUser(userID: number, transaction: Transaction): Promise<number> {
    return this.userPersistence.destroy({
      where: {
        id: userID
      },
      transaction: transaction
    });
  }

  async deleteUserMedia(
    userID: number,
    transaction: Transaction
  ): Promise<number> {
    return this.userMediaPersistence.destroy({
      where: {
        userID: userID
      },
      transaction: transaction
    });
  }

  async deleteUserAlbum(
    userID: number,
    transaction: Transaction
  ): Promise<number> {
    return this.userAlbumPersistence.destroy({
      where: {
        userID: userID
      },
      transaction: transaction
    });
  }

  getUserMedias(userID: number, transaction: Transaction) {
    return this.userMediaPersistence.count({
      where: { userID: userID },
      transaction: transaction
    });
  }
  getUserAlbums(userID: number, transaction: Transaction) {
    return this.userAlbumPersistence.count({
      where: { userID: userID },
      transaction: transaction
    });
  }

  searchUsers(userName: string) {
    return this.userPersistence.findAll({
      where: { name: { [Op.iLike]: `%${userName}%` } },
      attributes: ["id", "name", "email"]
    });
  }
}
